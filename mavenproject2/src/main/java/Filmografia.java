
import java.util.Random;

public class Filmografia {
	 protected String titulo;
	 protected String genero;
	 protected String año;
	 int cantidadMaximaEspectadores = 10001;
	 int cantidadMinimaEspectadores = 150;
	 Random especRandom = new Random();
	 protected int espectadores = especRandom.nextInt(cantidadMaximaEspectadores-cantidadMinimaEspectadores)+cantidadMinimaEspectadores;
	 
	public Filmografia (String titulo, String genero, String año ){
		this.titulo = titulo;
		this.genero = genero;
		this.año = año;
	}	
}